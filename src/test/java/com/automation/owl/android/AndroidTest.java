/**
 * 
 */
package com.automation.owl.android;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.seleniumhq.jetty9.server.HomeBaseWarning;
import org.testng.annotations.Test;

import com.owl.framework.BaseTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import pageObjects.HomePageObjects;
import pageObjects.LoginPageObjects;

/**
 * @author kdhillon
 *
 */
public class AndroidTest extends BaseTest{



	/**
	 * @param args
	 * @throws MalformedURLException 
	 * @throws InterruptedException 
	 */
	HomePageObjects homeObj;
	LoginPageObjects loginObj;

	@Test

	public void testAndroid() throws InterruptedException {
			String usernmae=getPropValue("Username");
			String password=getPropValue("Password");
		homeObj=new HomePageObjects(driver);
		loginObj=new LoginPageObjects(driver);
		homeObj.clickOnSignInLink();
		loginObj.userNameTextField(usernmae);
		Thread.sleep(2000);
		loginObj.passwordTextField(password);
		loginObj.signInButton();
		loginObj.verifyWarningMessage();
		loginObj.clickOkButton();
	}


}