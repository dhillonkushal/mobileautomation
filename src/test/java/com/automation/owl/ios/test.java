/**
 * 
 */
package com.automation.owl.ios;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

/**
 * @author kdhillon
 *
 */
public class test {
	protected static AppiumDriver<MobileElement> driver;

	/**
	 * @param args
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {
	
		DesiredCapabilities caps = new DesiredCapabilities();
		
		caps.setCapability("platformName", "iOS");

		caps.setCapability("platformVersion", "15.2.1"); // iPhone real device Personal
		
		caps.setCapability("deviceName", "iphone13"); //for Simulator
		
		caps.setCapability("app", "/Users/kdhillon/Downloads/Runner/form_app.ipa");
		caps.setCapability("automationName", "XCUITest");
		caps.setCapability("UDID", "00008110-001A0C4002F9801E");
		
		
		driver = new AppiumDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println("Session created");
		
		
		
		
	}

}
