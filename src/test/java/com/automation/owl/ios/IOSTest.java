/** 
 */
package com.automation.owl.ios;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.owl.framework.BaseTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

/**
 * @author kdhillon
 *
 */
public class IOSTest extends BaseTest {
	
	

	/**
	 * @param args
	 * @throws MalformedURLException 
	 * @throws InterruptedException 
	 */
	
	@Test
	public void iosTest() throws InterruptedException, MalformedURLException {
		// TODO Auto-generated method stub
	   
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		System.out.println("session created");
		
		Thread.sleep(5000);
		driver.findElementByAccessibilityId("Sign in with HTTP").click();
		Thread.sleep(5000);
		driver.findElementByAccessibilityId("Email Your email address").click();
		Thread.sleep(5000);
		driver.findElementByAccessibilityId("Password").click();
		Thread.sleep(5000);
		driver.findElementByAccessibilityId("Sign in").click();
		Thread.sleep(5000);
		driver.findElementByAccessibilityId("Unable to sign in.").isDisplayed();
		driver.findElementByAccessibilityId("OK").click();		
		
	}
	
	
}