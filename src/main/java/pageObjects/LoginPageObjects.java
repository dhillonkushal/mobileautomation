/**
 * 
 */
package pageObjects;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

/**
 * @author kdhillon
 *
 */
public class LoginPageObjects {

	AppiumDriver<MobileElement> driver;


	public LoginPageObjects(AppiumDriver<MobileElement> driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}


	
	


	public void userNameTextField(String username) {
		driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.widget.EditText[1]").sendKeys(username);

	}

	public void passwordTextField(String password) {
		driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.widget.EditText[2]").sendKeys(password);
		                       //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.widget.EditText[2]
	}
	public void signInButton() {
		driver.findElementByAccessibilityId("Sign in").click();
	}
	public void verifyWarningMessage() {
		driver.findElementByAccessibilityId("Unable to sign in.").isDisplayed();
	}
	public void clickOkButton() {
		driver.findElementByAccessibilityId("OK").click();

	}
	
}
