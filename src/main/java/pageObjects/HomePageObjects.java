/**
 * 
 */
package pageObjects;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

/**
 * @author kdhillon
 *
 */
public class HomePageObjects {
	
	AppiumDriver<MobileElement> driver;


	public HomePageObjects(AppiumDriver<MobileElement> driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	public void clickOnSignInLink() {
		
		driver.findElementByAccessibilityId("Sign in with HTTP").click();
	}
}
