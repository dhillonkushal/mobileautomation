package com.owl.framework;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BaseTest {

	Properties properties ;
	
	
	
	
	protected static AppiumDriver<MobileElement> driver;

	@BeforeMethod
	public void setup() throws MalformedURLException {
		loadPropertiesFile();
		String deviceType=getPropValue("deviceType");
		System.out.println(deviceType);
		DesiredCapabilities caps = this.getDesirecapabilities(deviceType.toUpperCase());

		driver = new AppiumDriver<MobileElement>(new URL("http://127.0.0.1:4725/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println("Session created");

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	public DesiredCapabilities getDesirecapabilities(final String deviceType) {
		DesiredCapabilities caps = new DesiredCapabilities();

		String aDeviceName=getPropValue("ADeviceName");
		String iDeviceName=getPropValue("IDeviceName");
		switch (deviceType) {
		
		case "IOS":
			caps.setCapability("appium:platformName", "iOS");

			caps.setCapability("appium:platformVersion", "12.5.5"); // iPhone real device Personal
			//caps.setCapability("deviceName", "iPhone"); // iPhone real device Personal
			//caps.setCapability("UDID", "b1612234d0295f58187dd825012db4703b9b0533"); // iPhone real device Personal
			

			// caps.setCapability("platformVersion","15.2.1"); //iPhone real device Owl Labs
			// caps.setCapability("deviceName","iphone13"); //iPhone real device Owl Labs
			// caps.setCapability("UDID", "00008110-001A0C4002F9801E"); //iPhone real device
			// Owl Labs
			
			caps.setCapability("appium:deviceName",iDeviceName); //for Simulator
			//caps.setCapability("appium:bundleId", "dev.flutter.formApp.formApp");
			
			caps.setCapability("appium:app", "/Users/kdhillon/Downloads/Runner/form_app.ipa");
			caps.setCapability("appium:automationName", "XCUITest");
			caps.setCapability("appium:UDID", "b1612234d0295f58187dd825012db4703b9b0533");
			//caps.setCapability("newCommandTimeout", "700");
			break;
			
		case "ANDROID":
			caps.setCapability("platformName", "android");
			caps.setCapability("platformVersion", "12");
			caps.setCapability("deviceName",aDeviceName);
			caps.setCapability("UDID", "1B011FDEE00304");

			caps.setCapability("app",
					"/Users/kdhillon/Downloads/samples-master/form_app/build/app/outputs/flutter-apk/app.apk");

			caps.setCapability("automationName", "UiAutomator2");
			caps.setCapability("newCommandTimeout", "700");
			caps.setCapability("noReset", "true");
			break;
		default:
			throw new RuntimeException("Please provide valida device type");

		}
		return caps;

	}
	
	public String getPropValue(String attributeName){
		String atVal = properties.getProperty(attributeName);
		if(atVal!= null) return atVal;
		else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");		
	}
	public void loadPropertiesFile() {
		BufferedReader reader;
		try {
			String sysDir=System.getProperty("user.dir");
			
			reader = new BufferedReader(new FileReader(sysDir+"/Config.properties"));
			properties = new Properties();
			properties.load(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}	

}


